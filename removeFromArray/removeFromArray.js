const removeFromArray = function(arr) {

    let result = arr;

    if(arguments.length <= 1){
        console.log("There are no elements to remove or even no array to remove from.");
    }
    else{
        for(let i=1; i<arguments.length; i++){
            

            let index = result.indexOf(arguments[i]);
            
            if(index != -1){
                result.splice(index,1);
            }
            else{
                console.log("Couldn't find "+arguments[i])
            }

        }
    }

    return result;
}

module.exports = removeFromArray
