const repeatString = function(text, times) {
    let result = "";

    if(times < 0){
        return "ERROR"
    }
    else{
        for (let i=0; i<times; i++){
            result = result.concat(text);
        }
    
        return result;
    }    
}

module.exports = repeatString
