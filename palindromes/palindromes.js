const palindromes = function(text) {
    let reversedText = "";
    var better = text.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,"");
    var best = better.replace(/\s{2,}/g," ");
    var best = best.toLowerCase();

    console.log(best)

    for (let i=best.length-1; i>=0; i--){
        reversedText = reversedText.concat(best.charAt(i));
    }

    if(reversedText === best){
        return true;
    }

    return false;
}

module.exports = palindromes
