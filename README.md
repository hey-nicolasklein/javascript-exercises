# javascript-exercises

~Odin Project JS exercises regarding arrays, data structures and more.

[The original exercises can be found here](https://github.com/TheOdinProject/javascript-exercises)