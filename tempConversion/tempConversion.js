const ftoc = function(celsius) {
  result = (celsius - 32) * 5/9;
  return round(result,1);
}

const ctof = function(celsius) {
  result = (celsius * 9/5) + 32;
  return round(result, 1);
}

function round(value, precision) {
  var multiplier = Math.pow(10, precision || 0);
  return Math.round(value * multiplier) / multiplier;
}

module.exports = {
  ftoc,
  ctof
}
