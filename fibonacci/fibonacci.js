const fibonacci = function(num) {
    let fib1 = 1;
    let fib2 = 1;

    for (let i=1; i<num; i++){
      let p = fib1;
      fib1 = fib2;
      fib2 = p+fib2;
    }

    return fib1;
}

module.exports = fibonacci
